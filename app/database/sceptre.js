
var deckCollection = db.briscas.decks;

//Function that creates function that returns next ids
function CreateIdGenerator(currentId)
{
	return {
		add: function()
		{
			currentId++;
			return currentId;
		},
		count: function()
		{
			return currentId;
		}
	}
}
function CountByKey(idField, callback)
{
	var filter = `${idField} >`;
	var list = deckCollection.findDocSync({filter : 0});
	
	if (list.length > 0)
	{
		callback(list.length);
	}

	return list.length;    
}

var count = CountByKey('deck_id');
function sceptre()
{
	var counter = CreateIdGenerator(count);
	this.idGenerator = counter.add;
	this.Count = counter.count;
	this.Get = function(id_key, id, callback)
	{
		var returns = deckCollection.findDocSync({id_key : id});

		if (returns[0] == null)
		{
			return null;
		}
		return callback(returns[0]);
	};
	this.Where = function(filter)
	{
		return deckCollection.findDoc(filter);
	}
	this.Update = function(newRow)
	{
		var updateRow = function(originalRow)
		{
			for (var attrname in newRow) { originalRow[attrname] = newRow[attrname]; }        
			deckCollection.saveDoc(originalRow);
		}
		
		this.Get('deck_id', newRow.deck_id, updateRow);
	};
	this.Save = function(row)
	{
		var index = this.idGenerator();
	
		row['deck_id'] = index;
		deckCollection.saveDoc(row);
	
		return row;
	};
}

module.exports = new sceptre();