create schema briscas
create table briscas.decks(
  id serial primary key,
  body jsonb not null,
  search tsvector,
  created_at timestamptz default now()
);
create index idx_briscas_decks on briscas.decks using GIN(body jsonb_path_ops);
create index idx_briscas_decks_search on briscas.decks using GIN(search);