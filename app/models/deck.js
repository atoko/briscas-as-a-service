var engine = require('../engine/briscas');

// Creates, Shuffles, and Returns an array of n cards.
function Range(n) {
    var range = [];
    for (var i = 0; i < n; i++)
    {
        range[i] = (i);
    }
    return range;
}

function Shuffle(array)
{
    for (var c = array.length - 1; c > 1; c--)
    {
        var r = (Math.random() * c).toFixed(0);
        var one = array[r];
        var two = array[c];

        if (one !== null)
        {
            array[c] = one;
            array[r] = two;
        }
    }

    return array;
}

function model(data)
{
    // 'Class' methods
    //
    this.Result = function()
    {
        var winner = this.engine.GetWinner(this.dropCards, this['lastWinner'], this.players.length);

        this['lastWinner'] = winner;
        for (card in this['dropCards'])
        {
            this['players'][winner]['grave'].push(this['dropCards'][card]);
            this['players'][winner]['points'] = this.engine.TotalPoints(this['players'][winner]['grave']);
        }
        this['dropCards'] = [];
        
        this['nextToDraw'] = winner;
        this['nextToPlay'] = winner;
        this.DealCards();
    }

    this.Turn = function()
    {
        var players = this['players'].length;

        this['nextToPlay'] = (this['nextToPlay'] + 1) % players;
        if (this['dropCards'].length >= players)
        {
            this.Result();
        }
    };

    this.Play = function(player_id, card_id)
    {
        if (player_id != this.players[this['nextToPlay']]['player_id'])
        {
            return {response: false, message: "Not your turn", params:[player_id, card_id, this['nextToPlay']]};
        }

        var playerHand = this['players'][this['nextToPlay']]['hand'],
            index = playerHand.indexOf(parseInt(card_id)),
            found = false;

        index = (card_id == 'r') ? 0 : index;

        if (index != -1)
        {
            card_id = playerHand[index];
            found = true;

            this['players'][this['nextToPlay']]['hand'].splice(index, 1);
            this['dropCards'].push(card_id);
            this['playSequence'].push(card_id);
        }

        return {response: found, message: card_id, state: this};
    };

    this.DealCards = function()
    {
        var playerCount = this['players'].length;

        if (this['deck'].length < playerCount)
            return;

        for (var p = 0; p < playerCount; p++)
        {
            var nextId = this['nextToDraw'];
            var card = this['deck'].pop();

            this['players'][nextId]['hand'].push(card);
            this['nextToDraw'] = (this['nextToDraw'] + 1) % playerCount;
        }
    };

    //Constructor code
    //
    if (data !== null)
    {
        for (var key in data)
        {
            switch (key)
            {
                case 'engine':
                    this['engine'] = new engine(data['engine']['life']);
                    break;
                default:
                    this[key] = data[key];
            }
        }
    }
    else
    {
        this['deck'] = Shuffle(Range(40));
        this['playSequence'] = [];
        this['players'] = [{'player_id': -1, 'hand' : [], 'grave' : [], 'points' : 0}, {'player_id': -1, 'hand' : [], 'grave' : [], 'points' : 0}];
        this['dropCards'] = [];
        this['lastWinner'] = 0;
        this['nextToDraw'] = 0;
        this['nextToPlay'] = 0;
        this['engine'] = new engine(null);
        
        this.engine.life = this.engine.DetermineSuit(this.deck[0]);

        //Deal 3 cards to each player
        for (var cards = 0; cards < 3; cards++)
        {
            this.DealCards();
        }
    }
}

module.exports = model;