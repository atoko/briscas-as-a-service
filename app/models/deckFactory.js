var sceptre = require('../database/sceptre');
var deckModel = require('./deck');

var Summon = function(deck_id, player_id, name)
{
	if (deck_id !== null)
	{
	    var deck = sceptre.Get('deck_id', deck_id, function(data)
	    {
			player_id = data == null ? null : player_id;
	        var deck = new deckModel(data, player_id);
			if (player_id && deck.players[1].player_id == -1)
			{
				deck.players[1]['player_id'] = player_id;
			}
	        return deck;
	    });
	    
	    return deck;
	}
	else
	{
		var deck = new deckModel(null);
		deck.name = name;
		deck.players[0]['player_id'] = player_id;
	    var data = sceptre.Save(deck);		
		return data;
	}
}


module.exports = Summon;