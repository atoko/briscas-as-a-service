var express = require('express');
var browserify = require('browserify-middleware');
var path = require('path');
var router = express.Router();

//Javascript
var productionOptions = { 
    cache: true,
    precompile: true,
    minify: true,
    gzip: true,
    debug: false,
    transform: [
      ['babelify', {presets: ['es2015', 'react']}],
      'uglifyify'
    ]
  };
  
var devOptions = {
    cache: 'dynamic',
    precompile: true,
    minify: false,
    gzip: true,
    debug: true,
    transform: [
      ['babelify', {presets: ['es2015', 'react']}]
    ],
    plugins: [
      {plugin: 'minifyify', options: {}}
    ]
  };
  
router.get('/js/boot.js', browserify(path.join(__dirname, '../views/index.js'), devOptions));

module.exports = router;