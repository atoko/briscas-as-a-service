var express = require('express');
var router = express.Router();
var query = function(){}//require('../database/postgres').Query;

router.post('/register', function(req, res, next){
	query(`
		select membership.register(
			'${req.body.username}',
			'${req.body.password}',
			'${req.body.confirm}'
		)`,
		function(response){
			response = response[0]['register'];			
			if (response.success)
			{
				var s = req.session;
				s.data = s.data ? s.data : {};
				s.data['player_id'] = response['new_id'];
				s.save();
				res.redirect('/');							
			}
			else
			{
				res.redirect('/register?message=' + response['message']);
			}		
		}
	);   	
})

router.post('/login', function(req, res, next){
	query(
		`select membership.authenticate(
			'${req.body.username}',
			'${req.body.password}',
			'local',
			'127.0.0.1'
		)`,	
		function(response){
			response = response[0]['authenticate'];
			
			if (response.success)
			{
				//save login id into player_id
				var s = req.session;
				s.data = s.data ? s.data : {};
				s.data['player_id'] = response['return_id'];
				s.data['name'] = req.body.username;
				s.save();
				res.redirect('/');							
			}
			else
			{
				res.redirect('/login?message=' + response['message']);
			}
		}
	);
})
router.get('/verify/:sess', function(req, res, next) 
{
    req.sessionStore.get(req.params.sess, function(error, session)
    {
        if (error)
        {
            res.send(error);
        }   
        res.json({result: session || false});        
    });
});

router.get('/logout', function(req, res, next)
{
    if (req.session)
	{
		req.session.data = {};
        req.session.destroy();
	}
    res.redirect('/');
}
);


module.exports = router;