var express = require('express');
var router = express.Router();
var sceptre = require('../database/sceptre');

router.get('/:player_id', function(req, res, next)
{
   req.playerId = req.params.player_id;
   next();
});

router.get('*', function(req, res, next)
{
    var getUserGames = function(playerId)
    {
        return {players: [{player_id: playerId}]};
    }
    var response = {player: [], open: []},
        playerId = req.playerId;
    if (req.session.data)
    {
        playerId = req.session.data['player_id'];
    }
    if (playerId)
    {
        response['player'] = sceptre.Where(getUserGames(playerId));        
    }
    
    response['open'] = sceptre.Where(getUserGames(-1))
        .filter((v) => {
            return !Object.keys(v["players"]).map((k) => v["players"][k])
                .reduce((o, n) => {return o || n['player_id'] == playerId} , false)
    });
    
    var ids = Object.keys(response)
        .map((v) => {return response[v];})
        //reduce each list
        .reduce((previous, list, index) => { 
            //reduce each game in list
            return previous + (previous === '' ? '' : ',') + list.reduce((previous, game, index) => {
                //reduce each player in game
                return previous + (previous === '' ? '' : ',') + game['players'].reduce((previous, player, i) => {
                    var playerId = player['player_id'];
                    if (!isNaN(playerId))
                    {
                        return previous + `${previous === '' ? '' : ','}${playerId}`;
                    }
                    
                    return previous;
                }, "")
            }, "");
        }, "");
    if (ids.length > 0) {
        if (ids.slice(-1) == ',')
        {
            ids = ids.slice(0, -1);
        }        
        ids.substr(0, ids.length - 1);
        /*
        var queryString = "SELECT id, email FROM membership.members WHERE id IN (" + ids + ")";
        query(queryString, function(data)
        {
            var flattened = {};
            flattened[-1] = "Waiting for player..";
            flattened[-2] = "Waiting for friend..";
            flattened[-3] = "Brisca Bot";                
                            
            data.forEach(function(element) {
                flattened[element['id']] = element['email'];
            }, this);
                    
            for (var group in response)
            {
                response[group].forEach(function(game) {
                    game.timeStamp = game.meta.updated || game.meta.created;
                    game.nextPlayer = flattened[game["players"][game.nextToPlay]['player_id']]
                    game.roomName = game.name;    
                    game["players"].forEach(function(player) {
                        if (flattened[player['player_id']] != null)
                        {
                            player['player_id'].name = flattened[player['player_id']];
                        }
                        else
                        {
                            player['player_id'].name = playerId;
                        }
                    })
                });  
            }
            res.json(response);        
        })  
        */
        res.json(response);          
    }
    else
    {
        res.json(response);
    }  
});

module.exports = router;