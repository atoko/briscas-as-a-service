var express = require('express');
var router = express.Router();
var decks = require('../models/deckFactory');
var sceptre = require('../database/sceptre');

var extractPlayerId = function(req, res, next)
{
    if(req.session.data)
	{
        req.playerID = req.session.data.player_id;
    }
    else
    {
        req.playerID = req.sessionID;  
    }
   
   next(); 
}
router.post('*', extractPlayerId);
router.get('*', extractPlayerId);
// Generate new deck
router.post('/_?', function(req, res, next)
{
    var deck, 
        playerId = req.playerID;
    
    var deck = decks(null, playerId, req.body.room);
    
    req.redirect = `game/${deck.deck_id}`;
    req.payload = JSON.stringify(deck);
    next();
});

router.get('/deckAssets/:pack_id', function(req, res, next) {
    var list = {};
    for (var i = 0; i < 40; i++) {
        list[i] = req.headers.host + '/assets/decks/fournier/' + i;
    };
    res.send(JSON.stringify(list))
});

router.get('/max', function(req, res, next){
    res.send(JSON.stringify(decks(1))); 
});

router.get('/:deck_id', function(req, res, next)
{
    var data = decks(req.params.deck_id),
        playerId = req.playerID;
    if (data == null)
    {
        data = decks(null, playerId);
    }
	
    var player = data.players.filter(function(p){ return p['player_id'] == playerId; });
	if(player[0] != null)
	{
		data.isNext = data.players[data.nextToPlay]['player_id'] == playerId;
		data.player = player[0];
	}

    res.send(JSON.stringify(data));    
});

// Generate new deck
router.get('/:deck_id/Join', function(req, res, next)
{
    var deck = decks(req.params.deck_id, req.playerID);
    
    if (deck)
    {
        sceptre.Update(deck);
        res.redirect('/game/' + deck.deck_id);
        return;   
    }
    
    res.redirect('/?message="invalid deck id"');
});

//Special case for serverside routing
// !! BAD
//
router.get('/:deck_id/:player_id', function(req, res, next)
{
    var data = decks(req.params.deck_id);
    
    if (data.deck_id == null)
    {
        data = decks(null);
    }
	
    var player_id = req.params.player_id;
    data.isNext = data.players[data.nextToPlay]['player_id'] == player_id;
    data.player = data.players.filter(function(p){ return p['player_id'] == player_id; })[0];
	

    res.send(JSON.stringify(data));    
});
//


router.get('/:deck_id/Play/:card_id', function(req, res, next)
{
    var deck = decks(req.params.deck_id);
    var playerId = req.playerID,
        card_id = req.params.card_id;
    
    var action = deck.Play(playerId, card_id);
    
    if (action.response)
    {
        if (deck.dropCards.length == deck.players.length)
        {
            setTimeout(function() {deck.Turn(); sceptre.Update(deck);}, 2000);
        }
        else
        {
            deck.Turn();
        }
        sceptre.Update(deck);

		action.state.nextToPlay = action.state.players[action.state.nextToPlay]['player_id'] == playerId;
		action.state.player = action.state.players.filter(function(p){ return p['player_id'] == playerId; })[0];
    }
    
    req.payload = JSON.stringify(deck);
    req.redirect = `game/${deck.deck_id}`;
    next();
});
var resolveRoute = function(req, res, next)
{
    if (req.url.slice(-1) == '_')
    {
        res.send(req.payload);            
    }
    else
    {
        res.redirect(`/${req.redirect}`);
    }
}
router.get("*", resolveRoute);
router.post("*", resolveRoute)
module.exports = router;