var express = require('express');
var router = express.Router();
var React = require('react');
var ReactDOMServer = require('react-dom/server');
var Router = require('react-router').RouterContext;
var MatchRoutes = require('react-router').match;
var Template = require('../views/index');

router.get('*', function(req, res, next) {
   var token = req.session.data ? req.session.data : null;
   req.session.routes = require('../views/routes')(((token) ? token.player_id : req.sessionID));
   req.session.save();
   next(); 
});

router.get('*', function(req, res, next) {
    var token = req.session.data ? req.session.data : null;
    var host = req.protocol + "://" + req.header('Host');
    var message = req.query.message ? req.query.message : '';

    global.host = host;
    
    function createWithDefaultProps(Component, props) {
        return  <Component {...props} message={message} hostname={host} token={req.sessionID} user={token}/>;
    }
    
    MatchRoutes( {routes: req.session.routes, location: req.path}, function(error, redirectLocation, renderProps) {
        if (error) {
            res.status(500).send(error.message);
        } else if (redirectLocation) {
            res.redirect(302, redirectLocation.pathname + redirectLocation.search);
        } else if (renderProps) {
            var html = `
            <html>
                <head>
                    <link href='https://fonts.googleapis.com/css?family=Raleway:400,300,500,700' rel='stylesheet' type='text/css'>
                    <link href='https://fonts.googleapis.com/css?family=Nunito:400,300,700' rel='stylesheet' type='text/css'>
                    <meta charset="UTF-8" />
                    <meta name="viewport" content="width=device-width,initial-scale=1">
                    <title>Spanish Poker</title>
                    <script src="/compiled/js/boot.js"></script>
                    <link rel="stylesheet" href="/stylesheets/game.css"></link>
                    <link rel="stylesheet" href="/stylesheets/normalize.css"></link>
                    <link rel="stylesheet" href="/stylesheets/skeleton.css"></link> 
                    <link rel="stylesheet" href="/stylesheets/index.css"></link>                                                        
                    <script>document.write('<script src="http://' + (location.host || 'localhost').split(':')[0] + ':35729/livereload.js?snipver=1"></' + 'script>')</script>
                    <script src="/js/bootstrap.js"></script>
                </head>
                <body>
                    <span style='display: none;' id = 'host-mount'>` + host + `</span>`
                    + `<span style='display: none;' id = 'session-mount'>` + (req.sessionID) + `</span>`
                    + `<span style='display: none;' id = 'message-mount'>` + (message ? message : '') + `</span>`    
                    + `<div id = 'react-mount'>`
                        + ReactDOMServer.renderToString(<Router createElement={createWithDefaultProps}  {...renderProps}/>)
                    +`</div>
                </body>
            </html>`;
        
            res.status(200).send(html);
        } else {
            res.status(404).send('Not found')
        }        
    });
});

module.exports = router;
