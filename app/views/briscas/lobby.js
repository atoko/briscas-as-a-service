var lobby = React.createClass({displayName:'Lobby',
    addGame: function(event)
    {
        debugger;
        fetch(global.host + '/baraja/_', {
            method:'post', 
            credentials: 'same-origin',  
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                room: document.getElementById("room").value(),
                action: this.state.action,
            })
        })
            .then(function(c){ return c.json()})
            .then(function(r) {
                if(this.props.history)
                {
                  this.props.history.push('game/' + r.deck_id);                    
                }
                else
                {
                  document.location = document.location;
                }
            }.bind(this)
        );
        
        event.preventDefault();
    },
    render: function() {
        var lobby =
            <section className={this.props.className}>
                <div className="login">

                    <h1>New game</h1>                
                    <form method="post"  onSubmit={this.addGame}>
                        {this.props.params.message}
                        {this.props.message}                    
                        <p><input className="u-full-width" type="text" id="room" name="room" placeholder="Room name" /></p>

                        <p className="submit"><input type="submit" value="Create a game" formAction="/baraja" onClick={function() {this.setState({action:'new'})}.bind(this)}/></p>
                        <p className="submit"><input type="submit" value="Play with a friend" formAction="/baraja/friend" onClick={function() {this.setState({action:'friend'})}.bind(this)}/></p>
                        <p className="submit"><input type="submit" value="Play with the computer" formAction="/baraja/cpu" onClick={function() {this.setState({action:'cpu'})}.bind(this)}/></p>
                    </form>
                </div>
            </section>;

        return (lobby);
    }
});

module.exports = lobby;
