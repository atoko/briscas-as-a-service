var Card = React.createClass({displayName: 'Card',
    post: function() {
        var url = global.host + '/baraja/' +  global.gameId +  '/Play/' + this.props.id;

        fetch(url, {credentials: 'same-origin'})
            .then(function(c){ return c.json()})
            .then(function(r) {
                if (r.response == true)
                {
                    this.props.onUpdate(r.state);
                }
                else
                {
                    alert(r.message);
                }
            }.bind(this));
    },
    render: function() {
        var respaldo = (isNaN(this.props.id) ? 'respaldo' : this.props.id + 1);
        var icon = React.createElement('img', {src: '/assets/decks/fournier/'  + respaldo + '.gif'});

        return (
            React.createElement('a', {className: "card", href: '/baraja/' +  global.gameId +  '/Play/' + this.props.id},//, onClick: this.post},
                icon,
                React.createElement('div', {className: "description"},  GenerateReadable(this.props.id)),
                React.createElement('div', {className: "help vida"})
            )
        );
    }
});
var DetermineSuit = function(card)
{
    return Math.floor(card / 10);
};
var GenerateSuit = function(card)
{    
    var faceText = ["Staffhs", "Cups", "Swords", "Pentacles"]
    return faceText[DetermineSuit(card)];
}
var DetermineFace = function(card)
{
    return card - (DetermineSuit(card) * 10);
};
var GenerateFace = function(card)
{    
    var denom = ["Ace", 2, 3, 4, 5, 6, 7, 'Squire' ,'Knight', 'King'];
    return denom[DetermineFace(card)];
}
var GenerateReadable = function(card)
{
    var suit = GenerateSuit(card);
    var number = GenerateFace(card);
    
    return isNaN(card) ? '' : number + " of " + suit;
}

var CardContainer = React.createClass({displayName: 'CardContainer',
    render: function() {
        var cards = [];
        if (this.props.cards.length > 0)
        {
            cards = this.props.cards.map(function(card, i) {
                return React.createElement(Card, {id: card, key: card + `_${i}`, onUpdate: this.props.onUpdate});
            }.bind(this));
        }
        else
        {
            cards = React.createElement('div', {});
        }
        return (
            React.createElement('form',
                {
                    className: "raise",
                    transitionName: "example",
                    transitionEnterTimeout: 500,
                    transitionLeaveTimeout: 300
                },
                cards
            ));
    }
});

var Player = React.createClass({displayName: 'Player',
    render: function() {
        if (!this.props.data)
        {
            return <div></div>
        }


        return (
            <div>
                <h1 className="pp">{this.props.data.name}</h1>
                <span>{this.props.data.isTurn && !this.props.data.isWait ? ' Your turn!': ''}</span>
                <div className="pp0">{this.props.data.points}</div>
                <CardContainer  cards={this.props.data.hand} onUpdate={this.props.data.onUpdate}/>
            </div>      
        );
    }
});

var Game =  React.createClass({displayName: 'Game',
    update: function(state) {

        if( state.dropCards != null)
        {
            state.wait = state.dropCards.length == state.players.length;
            state.loading = false;
            this.setState(state);
        }
    },
    getInitialState: function() {
        var gameData = this.props.params.game || {loading: true};
        return gameData;
    },
    render: function()
    {
        if (this.state.loading)
        {
            return React.createElement('div', { id: 'game-mount'}, "loading..");
        }

        var consoleDiv;

        if (this.state.player)
        {
            this.state.player.onUpdate = this.update;
            this.state.player.isTurn = this.state.isNext;
            this.state.player.isWait = this.state.wait;            
            
            this.state.opponent = this.state.players.filter((p, k) => p.player_id != this.state.player.player_id)[0];
            this.state.opponent.hand = this.state.opponent.hand.map((c) => {return 'respaldo';});
            

            if (this.state.playSequence.length >= 40)
            {
                return React.createElement('div', {className: 'container'}, this.state.player.points >= 60 ? 'You win!' : 'You lose');
            }
            
            consoleDiv = 
                <div>
                        <br/>
                        {this.state.player.hand.map((c, i) => {return <span key={i + "_cardtext"}> <br/> {GenerateReadable(c)}</span>;})}
                </div>;
        }
        else
        {
            //modal
            var text = `You are spectating. `;
            var onOK = function()
            {
                
            }
            if (this.state.players){}
            consoleDiv = <div>
                {text} 
                <a href={'/baraja/' + this.state.deck_id + '/Join'} className=''>
                    Click here to join
                </a>
            </div>
        }
        
        var waitDialog = (this.state.wait) ? React.createElement('div', {onClick:function(){ this.state.wait = false; this.poll(); }.bind(this)}, 'Click here to continue') : React.createElement('span');
        var trumpCard = this.state.deck.length > 0 ? <Card id={this.state.deck.slice(0)[0]} /> : '';
        var playerNames = this.state.players.map(function(p) { return p.name; })
        
        return <section className={this.props.className}> 
            <span style={{display:'none'}} id = 'game-data-mount'>{JSON.stringify(this.props.params.game)}</span>              

            <div className="game-container">
                <div className="main">
                    <h1>{this.state.name}</h1>               
                    {consoleDiv}
                </div>
                <div className="cards">
                    <CardContainer cards = {this.state.dropCards} />
                    {trumpCard}
                    {waitDialog}
                    <div className="raise">
                        <div className="deck">
                            <Card id="respaldo"/>
                            
                        </div>
                    </div>
                    <div className="spacer">
                        <div className="life">{'Trump: ' + GenerateSuit(this.state.engine.life)}</div>
                    </div>

                    <Player data = {this.state.opponent}/>          
                    <Player data = {this.state.player}/>  
                </div>
            </div>
        </section>;

    },
    componentWillMount: function() {
        global.gameId = this.props.params.gameId;
        this.poll();
    },
    componentDidMount: function() {
        this.state.timer = setInterval(this.poll, 4000);
    },
    componentWillUnmount: function() {
        clearInterval(this.state.timer);
    },
    poll: function() {
        if (!this.state.wait)
        {
            var url = global.host + '/baraja/' + global.gameId;

           fetch(url, {credentials: 'same-origin'})
                .then(function(c){ return c.json()})
                .then(function(r) {
                    this.update(r);
                }.bind(this));
        }
    }
});

module.exports = Game;