var gameList = React.createClass({displayName:'GameList',
    getInitialState: function() {
        var gameData = this.props.params.games || [];
        return {games: gameData};  
    },
    poll: function()
    {
        fetch(global.host + '/games/', {credentials: 'same-origin'})
            .then(function(c){ return c.json()})
            .then(function(r) {
                this.setState({games: r});
            }.bind(this));        
    },
    componentDidMount: function()
    {
        this.poll();
    },
    renderRows: function(source, name) {
        var structure = [];
        var list = [];
        if (!source)
        {
            return;            
        }
        structure.push(
            <thead key='ee'> 
                <tr>
                    <th className="u-nofloat three columns ">{name} Room</th>
                    <th className="u-nofloat three columns ">Next move</th>
                    <th className="u-nofloat three columns ">Last played</th>
                </tr>
            </thead>
        );
        
        source.forEach(function(element) {
            list.push(<tr key = {element.deck_id}>
                <td><Link to={'/game/' + element.deck_id}>{element.roomName}</Link></td>
                <td>{element.nextPlayer}</td>
                <td>{global.moment(element.timeStamp).fromNow()}</td>
            </tr>)
        }, this);    
        
        structure.push(<tbody key='dd'>{list}</tbody>);
        
        return structure;    
    },
    render: function() {
        var lobbyButton = (<form action='lobby'><input type="submit" value="+"/></form>);
        
        var games = []
        Object.keys(this.state.games).forEach(function (key) {
            var value = this.state.games[key];
            if (value.length > 0)
            {
                games.push(<table className='u-full-width' key={`list-${key}`}>{this.renderRows(value, key)}</table>);                
            }
        }.bind(this));
        
        return (
        <section className={this.props.className}>
        <span style={{display: 'none'}} className='sessionState' id = 'gamelist-data-mount'>{JSON.stringify(this.state.games)}</span>       
            <div className="login">
                <h1>Lobby</h1> 
                
                {games}
                {lobbyButton}
            </div>
        </section>)
    }
});

module.exports = gameList;