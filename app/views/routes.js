function generateRoutes(playerId)
{
    var onEnter = function(props, replace)
    {
        if (typeof document !== 'undefined' && document !== null) 
        {
            global.host = document.getElementById('host-mount').innerHTML;    
        }
    }
    var onGameListEnter = function(playerId)
    {
        return function(props, replace, next)
        {
            if (global.query)
            {
                fetch(global.host + '/games/' + playerId, {credentials: 'same-origin'})
                    .then(function(c){ return c.json()})
                    .then(function(r) {
                        props.params.games = r;
                        next();
                });  
            }
            else
            {
                if (typeof document !== 'undefined' && document !== null && document.getElementById('gamelist-data-mount'))
                {
                    props.params.games = JSON.parse(document.getElementById('gamelist-data-mount').innerHTML);            
                }
                next();
            }
        }
    }
    var onGameEnter = function(playerId, gameId)
    {
        return function(props, replace, next)
        {
            var id = props.params.gameId || gameId;
            var path = `${id}/${playerId}`;
            if (props.params.gameId == null)
            {
                path = 'max';
                global.gameId = 'max';
            }
            
            if (global.query)
            {
                fetch(global.host + '/baraja/' + path, {credentials: 'same-origin'})
                    .then(function(c){ return c.json()})
                    .then(function(r) {
                        if (typeof r == 'undefined')
                        {
                            r = global.staticGame;
                        }
                        props.params.game = r;
                        next();
                    })
                    .catch(function(e) {
                        //console.log(e);
                        next();  
                    });  
            }
            else
            {
                if (typeof document !== 'undefined' && document !== null && document.getElementById('game-data-mount'))
                {
                    props.params.game = JSON.parse(document.getElementById('game-data-mount').innerHTML);            
                }
                next();
            }
        }
    };
    
    var indexComponents = {
        c1: global.Menu,
        c2: global.Game,
        c3: global.GameList
    }
    var f = function(props, replace, next) { 
        onEnter(props, replace);
        if (props.location.pathname == "/")
        {
            onGameEnter(playerId, 1).bind(this)(props, replace, next);        
        }
        else
        {
            next();
        }
    }.bind(this);
    return  [
    <Route path="/" component={global.Template} onEnter={f}>
        <IndexRoute components = {indexComponents}/>
        <Route path="login" components = {{c1: global.Menu, c2: global.Login, c3: global.Register}}/>
        <Route path="register" component = {global.Register}/>    
        <Route path="game/:gameId" components =  {{c1: global.Menu, c2: global.Game}} onEnter={onGameEnter(playerId)}/>
        <Route path="gameList" component = {global.GameList} onEnter={onGameListEnter(playerId)}/>
        <Route path="lobby" components = {{c1: global.Menu, c2: global.GameList, c3: global.Lobby}}/>    
    </Route>]    
}
module.exports = generateRoutes;