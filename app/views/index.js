//Include client side libraries
global.React = require('react');
global.ReactDOM = require('react-dom');

require('es6-promise').polyfill();
require('isomorphic-fetch');

//Include react components
global.Game = require('./briscas/game.js');
global.GameList = require('./briscas/gameList.js');
global.Lobby = require('./briscas/lobby.js');

global.Login = require('./user/login.js');
global.Register = require('./user/register.js');

global.Menu = React.createClass({displayName: 'menu',
    render: function()
    {
        var defaultLinks = [
            <Link to='/'><button>Home</button></Link>,            
            <Link to='/login'>Log in</Link>,
            <Link to='/register'>Register</Link>
        ];
        
        var loggedInLinks = [
            <Link to='/'><button>Home</button></Link>,
            <Link to='/gamelist'>My games</Link>,
            <Link to='/leaderboard'>Leaderboard</Link>,
            <a href='/session/logout'>Log Out</a>
        ];
        
        
        var links = (this.props.user) ? loggedInLinks : defaultLinks;
        //links = links.map(function(v, ii) { return <button className="menu" key={ii}>{v}</button>});
        
        return <section className={this.props.className + " menu"}>
                <div>Gypsy CaRds</div>
             {links} </section>;        
    }
})

global.Template = React.createClass({displayName: 'template',
    getInitialState: function()
    {
        return {message: this.props.message ? this.props.message : ''};
    },
    componentWillMount: function()
    {        
        global.host = this.props.hostname;
    },
    componentWillReceiveProps: function()
    {
        this.setState({message:''});
    },
    render: function() {
        var children =  this.props.children ? this.props.children : { 
            "c1": this.props.c1,
            "c2": this.props.c2,
            "c3": this.props.c3,
            "c4": this.props.c4,
            "c5": this.props.c5
        };
        
        if (children["c1"] != null)
        {
            var list = [];
            for(var child in children)
            {
                if (children[child] != null)
                {
                    list.push(React.cloneElement(children[child], {message: this.state.message, className: child}));
                }
            }            
            
            children = list;
        }
        
        return <div className='container'>
            {children}
        </div>;
        
    }
});

global.Router = require('react-router').Router;
global.Route = require('react-router').Route;
global.IndexRoute = require('react-router').IndexRoute;
global.History = require('react-router').browserHistory;
global.Link = require('react-router').Link;
global.Routes = require('../views/routes')();
module.exports = global.Template;

global.moment = require('moment');