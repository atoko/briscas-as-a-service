var register = React.createClass({displayName:'Register',
    getDefaultProps: function(){
        return {model: null}
    },
    render: function() {

        var loginForm =
            <section className={this.props.className}>        
                    <h1>Register</h1>                   
                    <form action="/session/register" method="post"> 
                        {this.props.message}                                       
                        <p><input className="u-full-width" type="text" name="username" placeholder="Email" /></p>
                        <p><input className="u-full-width" type="password" name="password" placeholder="Password" /></p>
                        <p><input className="u-full-width" type="password" name="confirm" placeholder="Confirm Password" /></p>
                        <p className="submit"><input className="u-full-width" type="submit" name="commit" value="Sign up" /></p>
                    </form>
            </section>;

        if (this.props.model != null)
        {
            loginForm =
            <div>
                Logged in as {this.props.model.name}!
            </div>
        }

        return (loginForm);
    }
});

module.exports = register;
