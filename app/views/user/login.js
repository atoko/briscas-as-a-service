var login = React.createClass({displayName:'Login',
    getDefaultProps: function(){
        return {model: null}
    },
    render: function() {

        var resetForm = <span></span>;
        /*
        <div className="login-help">
            <p>Forgot your password? <a href="index.html">Click here to reset it</a>.</p>
        </div>
        */
        var loginForm =
            <section className={this.props.className}>
                    <h1>Log in</h1>
                    <form method="post" action="/session/login">
                        {this.props.params.message}
                        {this.props.message}                    
                        <p><input className="u-full-width" type="text" name="username" placeholder="Email" /></p>
                        <p><input className="u-full-width" type="password" name="password" placeholder="Password" /></p>
                        <p className="remember_me">
                        <label>
                            <input type="checkbox" name="remember_me" id="remember_me" />
                            Remember me on this computer
                        </label>
                        </p>
                        <p className="submit"><input className="u-full-width" type="submit" name="commit" value="Login" /></p>
                    </form>

                {resetForm}
            </section>;

        if (this.props.model != null)
        {
            loginForm =
            <div>
                Logged in as {this.props.model.name}!
            </div>
        }

        return (loginForm);
    }
});

module.exports = login;
