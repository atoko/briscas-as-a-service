

function engine(life)
{
    this['life'] = life;
    this['pointMapping'] = 
    {
	    0:  11,
	    2:  10,
	    7:  1,
	    8:  2,
	    9:  3
	};
    
    this.GetWinner = function(cardArray, lastWinner, playerCount)
    {
        var lead = cardArray[0];
        var winner = lead;
    	for (var card in cardArray) 
    	{
    	    winner = this.DetermineWinner(winner, cardArray[card]);
    	}
  		return (cardArray.indexOf(winner) + lastWinner) % playerCount;
    };
    this.TotalPoints = function(cardArray)
    {
        var points = 0;
        for (var card in cardArray)
        {
            points += this.DeterminePointValue(cardArray[card]);
        }
        return points;
    };
    this.DetermineWinner = function(cardA, cardB)
    {
        var winner = cardA;
        if 
        (
            (!this.DetermineLife(cardA) && this.DetermineLife(cardB)) 
            || (this.DetermineSuit(cardB) == this.DetermineSuit(cardA))
        )
        {
            //If cardB is Life but cardA isn't winner = cardB;
            //If both points = 0, then highest face wins;
            if (this.DeterminePointValue(cardB) > this.DeterminePointValue(cardA))
            {
                winner = cardB;
            }
            
            if (this.DeterminePointValue(cardA) == this.DeterminePointValue(cardB))
            {
                if (this.DetermineFace(cardA) > this.DetermineFace(cardB))
                {
                    winner = cardA;
                }
                else
                {
                    winner = cardB;
                }
            }
        }
        
        return winner;
    };
    this.DetermineLife = function(card)
    {
        return this.DetermineSuit(card) == this['life'];
    };
    this.DetermineSuit = function(card)
    {
    	return Math.floor(card / 10);
    };
    this.DeterminePointValue = function(card)
	{
	    return this.pointMapping[this.DetermineFace(card)] || 0;
	};
	this.DetermineFace = function(card)
	{
	    return card - (this.DetermineSuit(card) * 10);    
	}
}
module.exports = engine;