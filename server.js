#!/usr/bin/env node
require("babel-register")({
  plugins: 'transform-react-jsx'
});
var debug = require('debug')('briscas:server');
var http = require('http');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var bodyParser = require('body-parser');
var express = require('express');
var port =  process.env.PORT || 8080;
var app = express();
var session = require('express-session');
var pgSession = require('connect-pg-simple')(session);
var pg = require('pg');
var cookieParser = require('cookie-parser');
var sass = require('node-sass-middleware');
var massive = require("massive");
//Settings////////
app.set('port', port);
//////////////////

var connectionString = "postgres://qhllgyem:ehuLNJA8Hw7ITmYGDVVNZcGWzSpWqTVM@nutty-custard-apple.db.elephantsql.com:5432/qhllgyem";
if (typeof process.env.DB_CONNECTION !== "undefined")
{
    connectionString = process.env.DB_CONNECTION;
}

//"postgres://postgres:password@104.196.106.45/postgres";
global.db =  massive.connectSync({connectionString : connectionString});

//Middleware//////
app.use(sass({
  src: '/stylesheets',
  root: path.join(__dirname, 'public'),
  outputStyle: 'compressed',
  prefix: '/stylesheets'
}));
app.use(express.static(path.join(__dirname, 'public')));
app.use(cookieParser('lembas'));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(session({
  store: new pgSession({
    pg : pg,
    conString : connectionString,
  }),
  secret: 'lembas',
  resave: false,
  saveUninitialized: false,
  cookie: {
    maxAge: null
  }
}));
//app.use(favicon(__dirname + '/public/favicon.ico'));
//////////////////

//Routes//////////
app.use('/baraja', require('./app/routes/baraja'));
app.use('/games', require('./app/routes/games'));
app.use('/session', require('./app/routes/session'));
app.use('/compiled', require('./app/routes/compiled'));
app.use('/', require('./app/routes/client'));
//////////////////

//Error handlers
app.use(function(err, req, res, next) {
  res.status(err.status || 500);
  res.send(err.toString());
});
//////////////////

var server = http.createServer(app);
//.\node_modules\.bin\browserify.cmd -t reactify .\app\views\index.js -o .\public\js\boot.build.js

/**
 * Event listener for HTTP server "error" event.
 */

function onError(error) {
  if (error.syscall !== 'listen') {
    throw error;
  }

  var bind = typeof port === 'string'
    ? 'Pipe ' + port
    : 'Port ' + port;

  // handle specific listen errors with friendly messages
  switch (error.code) {
    case 'EACCES':
      console.error(bind + ' requires elevated privileges');
      process.exit(1);
      break;
    case 'EADDRINUSE':
      console.error(bind + ' is already in use');
      process.exit(1);
      break;
    default:
      throw error;
  }
}

/**
 * Listen on provided port, on all network interfaces.
 */

server.listen(port);
server.on('error', onError);
server.on('connection', function(socket) {console.log('connected!');} );