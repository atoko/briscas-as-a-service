    document.addEventListener('DOMContentLoaded', function onLoad() {           
        function createWithDefaultProps(Component, props) {
            var properties = props;
            properties.hostname = document.getElementById('host-mount').innerHTML;
            properties.token = document.getElementById('session-mount').innerHTML;
            properties.message = document.getElementById('message-mount').innerHTML;
            return React.createElement(Component, properties);
        }        
        
        setTimeout(function(){ 
            ReactDOM.render(
                React.createElement(Router, {
                    history: History,
                    routes:Routes,
                    createElement: createWithDefaultProps,
                }),
                document.getElementById('react-mount')
            );
        }, 700)
    });