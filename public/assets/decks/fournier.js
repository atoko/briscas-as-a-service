var briscas = require('../.././briscas')
var engine = new briscas(null);

var mapping = function(card)
{
	if (card === null)
	{
		return 'respaldo.jpg';
	}
	
	var type = engine.DetermineSuit(card);
	switch (type)
	{
		case 0:
			type = 'Bas';
			break;
		case 1:
			type = 'Cop';
			break;
		case 2:
			type = 'Esp';
			break;
		case 3:
			type = 'Oro';
			break;
	}
	var face = engine.DetermineFace(card);
	return String(type) + String(face + 1) + '.gif';	
}
module.exports = mapping;